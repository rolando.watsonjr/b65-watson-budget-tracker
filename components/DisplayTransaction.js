import UserContext from '../UserContext'
import {useContext, useEffect, useState} from 'react'
import {Table, Button, Jumbotron, Card, Row, Col} from 'react-bootstrap'
import DeleteTransactionButton from './DeleteTransactionButton'
import EditTransactionButton from './EditTransactionButton'
import styles from '../styles/DisplayTransaction.module.css'

export default function DisplayTransaction(){
    const {user} = useContext(UserContext);
    console.log(user.id)
    const [transaction, setTransaction] = useState([])
    const [ incomeSum, setIncomeSum ] = useState([])
    console.log(incomeSum)
    const [ expenseSum, setExpenseSum ] = useState([])
    console.log(expenseSum)


    // // new array for balance per transaction
    // let balanceArr = transaction.map(record => record.amount),sum
    // balanceArr = balanceArr.map(recordAmount => sum = (sum || 0) + recordAmount)

    // // get current total balance
    // const currentBalance = balanceArr.slice(-1)
    let balance = incomeSum - expenseSum;
    useEffect(()=> {
        fetch(`${process.env.NEXT_PUBLIC_API_URL}/user/details`, {
            headers: {  
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            setTransaction(data.transactions)

             // sum of all income
             const incomeCalc = data.transactions
                .filter(transaction => transaction.categoryType === 'Income')
                .map(transaction => transaction.amount)
                .reduce((total, amount) => total + amount, 0)
            setIncomeSum(incomeCalc)

            // sum of all expenses
            const expenseCalc = data.transactions
                .filter(transaction => transaction.categoryType === 'Expense')
                .map(transaction => transaction.amount)
                .reduce((total, amount) => total + amount, 0)
            setExpenseSum(expenseCalc)

            })


    },[])

    
    // const mapTransaction = transaction.map(mappedTransaction => {
    //  console.log(mappedTransaction)
    // })




    
    return (
        <div>
            <Jumbotron className={styles.JumbotronStyle}>
                <Row>
                    <Col>
                        <Card style={{ width: '18rem' }}>
                            
                             <Card.Body className={styles.IncomeCardStyle}>
                              <Card.Title className={styles.IncomeTitle}>Income</Card.Title>
                                 <Card.Text className={styles.IncomeAmountStyle}>
                                  {incomeSum}
                                </Card.Text>
                                
                              </Card.Body>
                        </Card>
                    </Col>
                    <Col>
                        <Card style={{ width: '18rem' }}>
                             
                             <Card.Body className={styles.ExpenseCardStyle}>
                              <Card.Title className={styles.ExpenseTitle}>Expense</Card.Title>
                                 <Card.Text className={styles.IncomeAmountStyle}>
                                 {expenseSum}
                                </Card.Text>
                                
                              </Card.Body>
                        </Card>
                    </Col>
                     <Col>
                        <Card style={{ width: '18rem' }}>
                             
                             <Card.Body className={styles.IncomeCardStyle}>
                              <Card.Title className={styles.BalanceTitle}>Total Balance</Card.Title>
                                 <Card.Text className={styles.IncomeAmountStyle}>
                                {balance}
                                </Card.Text>
                                
                              </Card.Body>
                        </Card>
                    </Col>
                </Row>
            </Jumbotron>
                <Col>
            <Row>
            <Table>
        <thead>
            <tr>
                <th className={styles.InfoText}>Date Added</th>
                <th className={styles.InfoText}>Category Type</th>
                <th className={styles.InfoText}>Description</th>
                <th className={styles.InfoText}>Amount</th>
                <th className={styles.InfoText}>Edit</th>
                <th className={styles.InfoText}>Delete</th>
              
             
            </tr>
        </thead>
        <tbody>
        {transaction.map(transaction => {
          
            return (
                
                    <tr>
                            
                        <td className={styles.InfoText}>{transaction.dateAdded}</td>
                        <td className={styles.InfoText}>{transaction.categoryType}</td>
                        <td className={styles.InfoText}>{transaction.description}</td>
                        <td className={styles.InfoText}>{transaction.amount}</td>
                        <td>    
                            <EditTransactionButton transaction={transaction._id}/>
                        </td>  
                        <td>    
                            <DeleteTransactionButton transaction={transaction._id}/>
                        </td>
                    </tr>
                
                )
        })}
        </tbody>
        </Table>
                </Row>
        </Col>
        </div>
        )

        

}
                                        