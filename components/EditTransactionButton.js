import {useEffect, useState, useContext} from 'react'
import UserContext from '../UserContext'
import {Button} from 'react-bootstrap'
import Router from 'next/router'


export default function EditTransactionButton({transaction}){
	const [transactionId, setTransactionId] = useState()
	console.log(transaction)
	 useEffect(()=> {
        fetch(`${process.env.NEXT_PUBLIC_API_URL}/user/details`, {
            headers: {  
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            setTransactionId(data.transaction)
        })
    },[transactionId])


	const redirectToEditForm = (transaction) =>{
		Router.push({
			pathname: '/transaction/edit',
			query: {transaction}
		})
	}
	return (
		<div>
		<Button variant="success" className="mr-1" onClick={()=> redirectToEditForm(transaction)}>Edit</Button>
		</div>
		)
}