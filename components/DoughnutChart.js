import {useState, useEffect} from 'react'
import {Form} from 'react-bootstrap'
import {Doughnut} from 'react-chartjs-2'

export default function DoughnutChart({records}) {
		console.log(records)
		const allRecords = records.map(record => record.amount)
		const allLabels = records.map(record => `${record.description} (${record.categoryName})`)

		 const incomeRecords = records.filter(record => record.categoryType === 'Income').map(record => record.amount)
    const incomeLabels = records.filter(record => record.categoryType === 'Income').map(record => `${record.description} (${record.categoryName})`)

    // array and label of all expense records
    const expenseRecords = records.filter(record => record.categoryType === 'Expense').map(record => record.amount)
    const expenseLabels = records.filter(record => record.categoryType === 'Expense').map(record => `${record.description} (${record.categoryName})`)

    const [ chartAmounts, setChartAmounts ] = useState(allRecords)
    const [ chartLabels, setChartLabels ] = useState(allLabels)
    const [ labelSelector, setLabelSelector ] = useState('All Records')
     const colors = ["#4F7CAC","#ABDF75","#C0E0DE","#F3B3A6","#CEFDFF","#B98B82","#D6F9DD","#E4959E","#C5D6D8","#CB958E","#99F7AB","#7E78D2","#8E9DCC","#7D84B2","#9EEFE5","#4F7CAC","#ABDF75","#C0E0DE","#F3B3A6","#CEFDFF","#B98B82","#D6F9DD","#E4959E","#C5D6D8","#CB958E","#99F7AB","#7E78D2","#8E9DCC","#7D84B2","#9EEFE5","#4F7CAC","#ABDF75","#C0E0DE","#F3B3A6","#CEFDFF","#B98B82","#D6F9DD","#E4959E","#C5D6D8","#CB958E","#99F7AB","#7E78D2","#8E9DCC","#7D84B2","#9EEFE5","#4F7CAC","#ABDF75","#C0E0DE","#F3B3A6","#CEFDFF","#B98B82","#D6F9DD","#E4959E","#C5D6D8","#CB958E","#99F7AB","#7E78D2","#8E9DCC","#7D84B2","#9EEFE5"]

    useEffect(() => {
        setChartAmounts(allRecords)
        setChartLabels(allLabels)
        setLabelSelector('All Records')
    },[records])

	return (
        <>
        
        <Doughnut
            data={{
                datasets:[{
                    data: chartAmounts,
                    backgroundColor: colors,
                    borderColor: '#363636'
                }],
                labels: chartLabels
            }}

            redraw={false}
        />
        </>
    )
}


