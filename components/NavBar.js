import {useContext} from 'react'
import {Navbar, Nav} from 'react-bootstrap'
import Link from 'next/link'
import UserContext from '../UserContext'
import styles from '../styles/Login.module.css'

export default function NavBar(){
	const {user} = useContext(UserContext)
	// console.log(user.isAdmin)
	return(
	<Navbar className={styles.NavBarStyle} variant="dark" expand="lg">
		<Navbar.Brand href="#home">Budget Tracker</Navbar.Brand>
		<Navbar.Toggle aria-controls="basic-navbar-nav" />
		<Navbar.Collapse id="basic-navbar-nav">
		<Nav className="ml-auto">
			{

				(user.id !== null) ?
					(user.isActive !== true) ?
					<>
						
						<Link href="/logout"><a className="nav-link">Logout</a></Link>
					</>
						: 
					<>	
						
						<Link href="/transaction"><a className="nav-link">Dashboard</a></Link>
						<Link href="/trend"><a className="nav-link">Balance Trend</a></Link>
						<Link href="/profile"><a className="nav-link">Profile</a></Link>
						<Link href="/logout"><a className="nav-link">Logout</a></Link>
					</>
				:
				<>
					<Link href="/register"><a className="nav-link">Register</a></Link>
					<Link href="/login"><a className="nav-link">Login</a></Link>
				
				</>
			}

		</Nav>
	</Navbar.Collapse>
	</Navbar>
)
}