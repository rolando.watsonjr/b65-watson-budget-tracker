import {useContext, useState, useEffect} from 'react'
import UserContext from '../../UserContext'
import Head from 'next/head'
import {Container} from 'react-bootstrap'
import DoughnutChart from '../../components/DoughnutChart'

export default function index({records}) {
	const {user} = useContext(UserContext)
	const [transactions, setTransactions] = useState([])

	useEffect(()=> {
        fetch(`${process.env.NEXT_PUBLIC_API_URL}/user/details`, {
            headers: {  
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
           console.log(data.transactions)
           setTransactions(data.transactions)

            })
    },[])	




	return (
        <>
        <Head><title>Trends</title></Head>
        {
        (transactions.length < 1)
        ?   <Container className="error-container" fluid>
                <h1>No transaction found</h1>
            </Container>
        :   <DoughnutChart records={transactions} />
        }
        </>
    )
}