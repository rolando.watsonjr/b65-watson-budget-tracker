import {useEffect, useState, useContext} from 'react'
import UserContext from '../../UserContext'
import {Card, Button, Row, Col, ListGroup, ListGroupItem} from 'react-bootstrap'
import styles from '../../styles/Profile.module.css'
import Link from 'next/link'

export default function index(){
	const {user} = useContext(UserContext)
	const [firstName, setFirstName] = useState('')
	const [lastName, setLastName] = useState('')
	const [email, setEmail] = useState('')
	const [loginType, setLoginType] = useState('')

	useEffect(()=> {
        fetch(`${process.env.NEXT_PUBLIC_API_URL}/user/details`, {
            headers: {  
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data.firstName)
            setFirstName(data.firstName)
            setLastName(data.lastName)
            setEmail(data.email)
            setLoginType(data.loginType)
            })
    },[])
	return (
		<div>
		<Row>
		<Col>
		</Col>
			<Col className="d-flex justify-content-around">
				<Card style={{ width: '20rem' }}>
				  <Card.Img className={styles.AvatarPhoto} variant="top" src="/avatar.png" />
				  <Card.Body>
				    <Card.Title className="text-center">Profile</Card.Title>
				  </Card.Body>
				  <ListGroup className="list-group-flush">
				    <ListGroupItem className={styles.ProfileText}>First Name: {`  ${firstName}`}</ListGroupItem>
				    <ListGroupItem className={styles.ProfileText}>Last Name: {`  ${lastName}`}</ListGroupItem>
				    <ListGroupItem className={styles.ProfileText}>Email: {`  ${email}`}</ListGroupItem>
				    <ListGroupItem className={styles.ProfileText}>Login Type: {`  ${loginType}`}</ListGroupItem>
				  </ListGroup>
				  <Card.Body>
				<Button variant="success"><Link href="/transaction"><a>Back to Dashboard</a></Link></Button>
				  </Card.Body>
				</Card>
			</Col>
		<Col>
		</Col>
		
		</Row>
		</div>
		)
}