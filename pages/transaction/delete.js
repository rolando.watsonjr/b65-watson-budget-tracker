import {useEffect, useState, useContext} from 'react'
import UserContext from '../../UserContext'
import {Button, Row, Col, Card} from 'react-bootstrap'
import Router from 'next/router'
import styles from '../../styles/DeleteButton.module.css'
import Link from 'next/link'

export default function DeleteTransactionButton({transaction}){
	const {user} = useContext(UserContext)
	const [paramsTrId, setparamsTrId] = useState('')

	useEffect(() => {
		setparamsTrId(window.location.search.slice(13))
	}, [])
	// const [transactionId, setTransactionId] = useState()

	 // useEffect(()=> {
  //       fetch('http://localhost:4000/api/user/details', {
  //           headers: {  
  //               Authorization: `Bearer ${localStorage.getItem('token')}`
  //           }
  //       })
  //       .then(res => res.json())
  //       .then(data => {
  //           setTransactionId(data._id)
		
  //       })
  //   },[])

	const archive = (transaction) => {
		fetch(`${process.env.NEXT_PUBLIC_API_URL}/user/${user.id}/tr/${paramsTrId}`,{
			method: 'DELETE',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			if (data) {
				Router.push('/transaction')
			} else {
				Router.push('/error')
			}
		})
	}
	return (
		<div>
		<Row>
		<Col>
		</Col>
		<Col className={styles.CardStyle}>
		<Card style={{ width: '18rem' }} >
  				<Card.Body >
 			   <Card.Title>Are you sure you want to delete the selected transaction?</Card.Title>
  			  <Card.Text>
   			   You cannot undo once you press the delete button.
   			 </Card.Text>
   			 <Button variant="danger" onClick={()=> archive(transaction)}>Delete</Button>
   			 <br/>
   			 <br/>
   			 <Button variant="success"><Link href="/transaction"><a>Cancel Delete</a></Link></Button>
 			 </Card.Body>
			</Card>
			
		</Col>
		<Col>
		</Col>
		</Row>
		</div>
		)
}