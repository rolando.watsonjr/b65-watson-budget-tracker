import {useState, useEffect, useContext} from 'react'
import {Form, Button, Alert, Jumbotron, Row, Col} from 'react-bootstrap'
import UserContext from '../../UserContext'
import Router from 'next/router'
import Link from 'next/link'
import styles from '../../styles/CreateTransaction.module.css'

export default function create() {
	// return <h1>hello World</h1>
	const {user} = useContext(UserContext)
	const [categoryType, setCategoryType] = useState('')
	const [description, setDescription] = useState('')
	const [amount, setAmount] = useState('')
	const [dateAdded, setDateAdded] = useState('')
	const [isDisabled, setIsDisabled] = useState(true)
	// const {notify, setNotify} = useState('')
	// const {show, setShow} = useState('')

	// useEffect(()=> {
	// 	if (user.isActive == true) {
	// 		Router.push('/transaction')
	// 	}
	// },[])
	useEffect(()=>{
		if(categoryType !== '' && description !== '' && amount !== ''){
			setIsDisabled(false)
		} else {
			setIsDisabled(true)
		}
	},[categoryType, description, amount])

	function createTransaction(e) {
		e.preventDefault()

		fetch(`${process.env.NEXT_PUBLIC_API_URL}/user/${user.id}/transactions`, {
			method: 'PUT',
			headers: {
				'Content-Type' : 'application/json',
				'Authorization' : `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				categoryType: categoryType,
				description: description,
				amount: amount,
			})
		})
		.then(res => res.json())
		.then(data => {
			if (data) {
				setCategoryType('')
				setDescription('')
				setAmount('')
				Router.push('/transaction')
			} else {
				
			}
		})
	}
	// if (user.isActive == true) {
		return (
			<div>
			<Row>
			<Col xs={6} md={4}>
			</Col>
			<Col className="mt-4">
				<h1 className={styles.InfoText}>ADD TRANSACTION</h1>
				<Form onSubmit={e => createTransaction(e)}>
					<Form.Group>
						<Form.Label className={styles.InfoText}>Category Type</Form.Label>
						<Form.Control
						type="text"
						onChange={e => setCategoryType(e.target.value)}
						value={categoryType}
						placeholder="eg. Income, Expense"
						/>
					</Form.Group>
					<Form.Group>
						<Form.Label className={styles.InfoText}>Description</Form.Label>
						<Form.Control
						type="text"
						onChange={e => setDescription(e.target.value)}
						value={description}
						placeholder="eg. Salary, Transpo"
						/>
					</Form.Group>
					<Form.Group>
						<Form.Label className={styles.InfoText}>Amount</Form.Label>
						<Form.Control
						type="number"
						onChange={e => setAmount(e.target.value)}
						value={amount}
						placeholder="PHP"
						/>
					</Form.Group>
				<Button variant="success" type="submit" disabled={isDisabled}>Submit</Button>
				</Form>
				<Button className="mt-4" variant="danger"><Link href="/transaction"><a>Cancel transaction</a></Link></Button>
			</Col>
			<Col>
			</Col>
			</Row>
			</div>

			)
	// }
}