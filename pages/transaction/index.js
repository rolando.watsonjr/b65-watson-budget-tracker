import UserContext from '../../UserContext'
import {Jumbotron, Row, Col, Button} from 'react-bootstrap'
import {useContext} from 'react'
import Link from 'next/link'
import DisplayTransaction from '../../components/DisplayTransaction'
import styles from '../../styles/DisplayTransaction.module.css'	

export default function index() {

		const {user} = useContext(UserContext);
		console.log({user})
		return (
		<div>	
		<Jumbotron className={styles.JumbotronHeaderStyle}>
			<h1 className={styles.DisplayName}>Hello, {[user.firstName]}!</h1>
			<p className={styles.DisplayName}>To get started, please create your first transaction</p>
			<p>
    			<Button variant="success"><Link href="/transaction/create"><a>Add Transaction</a></Link></Button>
  			</p>
		</Jumbotron>
		<DisplayTransaction/>

		</div>	
		)
}