import {useState, useEffect, useContext} from 'react'
import {Form, Button, Alert, Row, Col} from 'react-bootstrap'
import Router from 'next/router'
import {useRouter} from 'next/router' 
import UserContext from '../../UserContext'
import Link from 'next/link'
import styles from '../../styles/CreateTransaction.module.css'


export default function edit(){
	const {user} = useContext(UserContext)
	const router = useRouter()
	const {transactionId} = router.query
	const [categoryType, setCategoryType] = useState('')
	const [transaction, setTransaction] = useState('')
	const [description, setDescription] = useState('')
	const [amount, setAmount] = useState('')
	const [isDisabled, setIsDisabled] = useState(true)
	const [notify, setNotify] = useState(false)
	const [watcher, setWatcher] = useState(false)
	const [paramsTrId, setparamsTrId] = useState('')
	useEffect(()=> {
		fetch(`${process.env.NEXT_PUBLIC_API_URL}/user/details`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data.transactions)
				setTransaction(data.transactions)
				setCategoryType('')
				setDescription('')
				setAmount('')


			// let dataTr = data.transactions
			// const selectedTrans = dataTr.findIndex(function(todo, index){
			// 	if (todo.categoryType === 'Income') {
			// 		return todo
			// 	} else {
			// 		false
			// 	}
			
		
			// console.log(selectedTrans)
		})
	}, [watcher])



	useEffect(()=> {
		if(categoryType !== '' && description !== '' && amount !== ''){
			setIsDisabled(false)
		} else {
			setIsDisabled(true)
		}
	}, [categoryType, description, amount])


	useEffect(() => {
		setparamsTrId(window.location.search.slice(13))
	}, [])

		
	function editTransaction(e){		
		e.preventDefault()
		fetch(`${process.env.NEXT_PUBLIC_API_URL}/user/${user.id}/tr/${paramsTrId}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization' : `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				categoryType: categoryType,
				description: description,
				amount: amount
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data) {
				Router.push('/transaction')
				setWatcher(true)
			} else {
				setNotify(true)
			}
		})
	}

	return (
		<div>
		<Row>
			<Col xs={6} md={4}>
			</Col>
			<Col className="mt-4">
		{(notify === true)?
			<Alert variant="danger">Failed to update transaction!</Alert>
		: null
		}
		<h1 className={styles.InfoText}>EDIT TRANSACTION</h1>
		<Form onSubmit={(e)=> editTransaction(e)}>
			<Form.Group>
						<Form.Label className={styles.InfoText}>Category type</Form.Label>
						<Form.Control
						type="text"
						onChange={e => setCategoryType(e.target.value)}
						value={categoryType}
						placeholder="Edit category type e.g Income, Expense"
						/>
					</Form.Group>
					<Form.Group>
						<Form.Label className={styles.InfoText}>Description</Form.Label>
						<Form.Control
						type="text"
						onChange={e => setDescription(e.target.value)}
						value={description}
						placeholder="Edit description e.g Salary, Transpo"
						/>
					</Form.Group>
					<Form.Group>
						<Form.Label className={styles.InfoText}>Amount</Form.Label>
						<Form.Control
						type="number"
						onChange={e => setAmount(e.target.value)}
						value={amount}
						placeholder="Edit amount"
						/>
					</Form.Group>
			<Button type="submit" variant="success" disabled={isDisabled}>Update</Button>

		</Form>
			<Button className="mt-4" variant="danger"><Link href="/transaction"><a>Cancel Update</a></Link></Button>
		</Col>
			<Col>
			</Col>
			</Row>
		</div>
		)
}