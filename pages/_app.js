import {useState, useEffect} from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import '../styles/globals.css'
import {Container} from 'react-bootstrap'
import Head from 'next/head'	
import NavBar from '../components/NavBar'
import { UserProvider } from '../UserContext'


function MyApp({ Component, pageProps }) {
	const [user, setUser] = useState({
		id: null,
		firstName: null,
		isActive: null
	})

	const unSetUser = () => {
		localStorage.clear()
		setUser({
			id: null,
			firstName: null,
			isActive: null

		})
	}

	useEffect(()=> {
		fetch(`${process.env.NEXT_PUBLIC_API_URL}/user/details`, {
			headers: {	
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			if (data._id) {
				setUser({
					id: data._id,
					firstName: data.firstName,					
					isActive: data.isActive
				})
			} else {
				setUser({
					id: null,
					firstName: null,
					isActive: null
				})
			}
		})
	}, [user.id])
  return (
  	<div>
  	<Head>
  		<title>B65 Budget Tracker</title>
  		<meta name="viewport" content="initial-scale=1.0, width=device-width" />
  	</Head>
  	<UserProvider value={{user, setUser, unSetUser}}>
  		<NavBar/>
  		<Container fluid className="main-container">
  			<Component {...pageProps} />
  		</Container>
  	</UserProvider>	
  	</div>
  	)

}

export default MyApp
