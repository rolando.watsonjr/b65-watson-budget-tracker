import { useState, useEffect } from 'react'
import {Row, Col, Form, Button} from 'react-bootstrap'
import Router from 'next/router'
import Swal from 'sweetalert2'
import styles from '../styles/CreateTransaction.module.css'


export default function register(){
	//form input state hooks
	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')
	const [verifyPassword, setVerifyPassword] = useState('')
	const [firstName, setFirstName] = useState('')
	const [lastName, setLastName] = useState('')
	const [mobileNumber, setMobileNumber] = useState('')
	//state to determine the status of form submit button
	const [isDisabled, setIsDisabled] = useState(true)

	useEffect(()=> {
		//validate the password: check if the passwords are not empty and are equal value
		if ((password !== '' && verifyPassword !== '') && (password === verifyPassword)) {
			setIsDisabled(false)
		} else {
			setIsDisabled(true)
		}
	}, [password, verifyPassword])

	//function for submitting user registration form
	function registerUser(e){
		e.preventDefault()
		// alert('You submitted!')
		fetch(`${process.env.NEXT_PUBLIC_API_URL}/user`, {
			method: 'POST', 
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				password: password,
				mobileNumber: mobileNumber
			})
		})
		.then(res => res.json())
		.then(data => {
			//if registration is successful, redirect to login
			if(data.error) {
				if (data.error == 'use-google-login') {
                      
                    Swal.fire(
                        'Google Account Registered',
                        'You may have registered through a different procedure.',
                        'error'
                        )
                } else if(data.error === 'account-already-registered') {
                       
                        Swal.fire(
                            'Already registered',
                            'Your email is already register on our app',
                            'error'
                            )
                }
			} else {
				Router.push('/login')
			}
		})
	}

	return(
		<Row>
			<Col xs={6} md={4} className="mx-auto">
				<h1 className={styles.InfoText}>REGISTRATION</h1>
				<Form onSubmit={(e) => registerUser(e)} className="mt-4">
					<Form.Group>
						<Form.Label className={styles.InfoText}>First name</Form.Label>
						<Form.Control
							 type="text"
							 onChange={e => setFirstName(e.target.value)}
							 value={firstName}
							 placeholder="Please put your First Name"
							 required>
						</Form.Control>
					</Form.Group>
					<Form.Group>
						<Form.Label className={styles.InfoText}>Last name</Form.Label>
						<Form.Control 
							type="text"
							onChange={e => setLastName(e.target.value)}
							value={lastName}
							placeholder="Please put your Last Name"
							required>
						</Form.Control>
					</Form.Group>
					<Form.Group>
						<Form.Label className={styles.InfoText}>Email</Form.Label>
						<Form.Control
							type="email"
							onChange={e => setEmail(e.target.value)}
							value={email}
							placeholder="Please put your Email Address"
							required>
						</Form.Control>
					</Form.Group>
					<Form.Group>
						<Form.Label className={styles.InfoText}>Mobile</Form.Label>
						<Form.Control
							type="text"
							onChange={e => setMobileNumber(e.target.value)}
							value={mobileNumber}
							placeholder="Please put your Mobile Number"
							required>
						</Form.Control>
					</Form.Group>
					<Form.Group>
						<Form.Label className={styles.InfoText}>Password</Form.Label>
						<Form.Control
							type="password"
							onChange={e => setPassword(e.target.value)}
							value={password}
							placeholder="Please put your Password"
							required>
						</Form.Control>
					</Form.Group>
					<Form.Group>
						<Form.Label className={styles.InfoText}>Verify Password</Form.Label>
						<Form.Control
							type="password"
							onChange={e => setVerifyPassword(e.target.value)}
							value={verifyPassword}
							placeholder="Verify Password"
							required>
						</Form.Control>
					</Form.Group>
					<Button variant="success" type="submit" disabled={isDisabled}>Register</Button>
				</Form>
			</Col>
		</Row>
		)	
} 