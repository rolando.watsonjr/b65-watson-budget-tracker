import React, {useState, useEffect, useContext} from 'react'
import {Row, Col, Form, Button, Container} from 'react-bootstrap'
import Router from 'next/router'
import UserContext from '../UserContext'
import { GoogleLogin } from 'react-google-login'
import Swal from 'sweetalert2'
import styles from '../styles/Login.module.css'

export default function login() {
	//use the UserContext and destructure it to obtain the setUser state setter from our app entry point
	const { setUser } = useContext(UserContext)

	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')
	const [isDisabled, setIsDisabled] = useState(true)

	useEffect(()=> {
		if(email !== '' && password !== '') {
			setIsDisabled(false)
		} else {
			setIsDisabled(true)
		}
		},[email, password])


	function authenticate(e){
		e.preventDefault()

		fetch(`${process.env.NEXT_PUBLIC_API_URL}/user/login`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data.accessToken) {
				//store Accesstoken in local storage
				localStorage.setItem('token', data.accessToken)
				//obtain user id and isAdmin properties for setting our globally scoped user state by sending a fetch request to the API endpoint
				fetch(`${process.env.NEXT_PUBLIC_API_URL}/user/details`, {
					headers: {
						'Authorization': `Bearer ${data.accessToken}`
					}
				})
				.then(res => res.json())
				.then(data => {
					// console.log(data)
					//set the global user state to have the proper id and isAdmin properties
					setUser({
						id: data._id
					})
					Router.push('/transaction')
				})
			} else {
				//authentication failure, redirect to error
				Router.push('/error')
			}
		})
	}

	 const captureLoginResponse = (response) =>{
        // console.log(response)
        fetch	(`${process.env.NEXT_PUBLIC_API_URL}/user/verify-google-id-token`, {
        	method: 'POST',
        	headers:{
        		'Content-Type': 'application/json'
        	},
        	body: JSON.stringify({tokenId:response.tokenId})
        })
        .then(res => res.json())
        .then(data => {
        	console.log(data)
        	// if data returned by api has an access token
        	if (typeof data.accessToken !== 'undefined') {
        		localStorage.setItem('token', data.accessToken)
        		setUser({id: data._id})
        		Router.push('/transaction')
        	} else { // else if the data return by api is an error msg
        		if (data.error == 'google-auth-error') {
                        // error == 'google auth error', return a Swal
                    Swal.fire(
                        'Google Auth Error',
                        'Google authentication procedure failed.',
                        'error'
                        )
                } else if(data.error === 'login-type-error') {
                        // error == 'login type error, return a Swal'
                        Swal.fire(
                            'Login Type Error',
                            'You may have registered through a different login procedure.',
                            'error'
                            )
                }
        	}
        })
    }

	return (
		<div>
		<Container>
	<Row>
		<Col xs={6} md={4} className="ml-auto">
			<Form onSubmit={e => authenticate(e)} className="mt-5">
				<Form.Group >
					<Form.Label className={styles.FormTitle}>Email:</Form.Label>
					<Form.Control
						type="email"
						onChange={e => setEmail(e.target.value)}
						value={email}
						placeholder="Please input your Email Address"

					/>
				</Form.Group>
				<Form.Group>
					<Form.Label className={styles.FormTitle}>Password:</Form.Label>
					<Form.Control
						type="password"
						onChange={e => setPassword(e.target.value)}
						value={password}
						placeholder="Please input your Password"
					/>
				</Form.Group>
				<Button variant="success" type="submit" disabled={isDisabled}>Login</Button>
			</Form>
			<div className="mt-4">
			<p className={styles.FormTextInfo}>Dont have Account Yet? Please click Register on the top-right of the page</p>
			<p className={styles.FormTextInfo}>For a convinience signup, Please Login as your google account.Please click below</p>
			<GoogleLogin 
				clientId='114161028365-jq6oepdjfnkbkbk1c3dc65327m32985g.apps.googleusercontent.com' 
	            onSuccess={captureLoginResponse} 
	            onFailure={captureLoginResponse} 
	            cookiePolicy={'single_host_origin'}
	         />
			</div>
		</Col>
	</Row>
		</Container>
		</div>
		)
}